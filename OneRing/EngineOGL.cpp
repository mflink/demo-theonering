#include "StdAfx.h"
#include "engineogl.h"

CEngineOGL::CEngineOGL(void)
{
	m_iExitEngine = 0;
	m_poLight = 0;
	m_poCamera = 0;
}

CEngineOGL::~CEngineOGL(void)
{
	m_iExitEngine = 1;
}

void CEngineOGL::Run(HWND hWindow)
{
	ShowCursor(0);

	// Intro abspielen (falls vorhanden)
	this->PlayIntro(hWindow);

	// OpenGL in das Hauptfenster einbetten
	if(!this->AttachToWindow(hWindow))
	{
		return;
	}

	// Initialisierung durchf�hren
	if(this->InitializeEngine())
	{
		// Hauptschleife:
//	TODO:	Diese Schleife ist nicht sehr effizient, da sie soviel 
//			CPU-Zeit verwendet wie m�glich, wenn das Fenster minimiert wurde.

		while(!m_iExitEngine)
		{
			this->DoOSEvents();
			this->ProcessInput();
			this->DrawScene();
		}
	}

	// Engine sauber verlassen
	this->Shutdown();
}

int CEngineOGL::InitializeEngine(void)
{
	int iLoop = 0;
	int iField = 0;

	// Schriftart laden
	m_oFont.LoadRasterfont_BMP("fonts/raster/RasterFont_Fixedsys.bmp", 95, ' ');
	m_oFont.SetColor(1.0f, 1.0f, 1.0f);

	// Startposition einstellen
	m_poCamera = (CCameraOGL *)malloc(iMAXIMUM_CAMERAS * sizeof(CCameraOGL));
	m_poCamera[iCAMERA_DEFAULT].SetPosition(0.0f, 0.0f, 0.0f);

	// Lichter erstellen
	m_poLight = (CLightOGL *)malloc(iMAXIMUM_LIGHTS * sizeof(CLightOGL));
	for(iLoop = 0; iLoop < iMAXIMUM_LIGHTS; iLoop++)
	{
		if(!m_poLight[iLoop].Create())
		{
			return 0;
		}
	}

	m_poLight[0].SetPosition(0.0f, 0.0f, 1.0f, 1.0f);
	m_poLight[0].SetColorDiffuse(1.0f, 1.0f, 1.0f, 1.0f);
	m_poLight[0].SetSpotCutoff(4.0f);
	m_poLight[0].Enable();

	m_poCamera[iCAMERA_DEFAULT].Rotate(0.0f, 0.0f, 0.0f);
	m_poCamera[iCAMERA_DEFAULT].Activate();

	m_oTheOneRing.LoadCOB(sFILENAME_THEONERING);
	m_oTheOneRing.LoadTextureMaps();
	m_oTheOneRing.GenerateDisplayList();

	return 1;
}

void CEngineOGL::InitializeScene(void)
{
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glFrontFace(GL_CW);
	glEnable(GL_CULL_FACE);
    glEnable(GL_NORMALIZE);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_COLOR_MATERIAL);
}

void CEngineOGL::DrawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	m_oFont.Printf(-535.0f, -390.0f, "FPS: %i", m_iFPS);

	m_poCamera[iCAMERA_DEFAULT].Record();

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	m_oTheOneRing.Render(0.0f, 0.0f, -0.5f);
	m_oTheOneRing.Rotate(/*m_oTheOneRing.m_tRotation.fX + 0.1f*/ -20.0f,
		m_oTheOneRing.m_tRotation.fY - 0.1f,
		m_oTheOneRing.m_tRotation.fZ);

	this->SwapSceneBuffers();
	m_iFPS = this->GetFramesPerSecond();
}

void CEngineOGL::ProcessInput(void)
{
///////////////////////////// FOR DEBUG ONLY !!! >
	if(this->KeyPressed('L'))
	{
		if(glIsEnabled(GL_LIGHTING))
		{
			m_poLight[0].Disable();
			glDisable(GL_LIGHTING);
		}
		else
		{
			m_poLight[0].Enable();
		}
	}
///////////////////////////// FOR DEBUG ONLY !!! <

 	if(this->KeyPressed(27))	// ESCAPE
	{
		m_iExitEngine = 1;
	}
}

void CEngineOGL::PlayIntro(HWND hWindow)
{
//	TODO:	Intro generieren und/oder abspielen lassen.
}

int CEngineOGL::KeyPressed(int iKeyNumber)
{
	short nState = GetAsyncKeyState(iKeyNumber) & 1;

	if(nState == 1)
	{
		return 1;
	}

	return 0;
}

int CEngineOGL::KeyDown(int iKeyNumber)
{
	short nState = GetAsyncKeyState(iKeyNumber) >> 15;

	if(nState)
	{
		return 1;
	}

	return 0;
}

void CEngineOGL::DoOSEvents(void)
{
	MSG tMessage;

	if(PeekMessage(&tMessage, NULL, 0, 0, PM_REMOVE))
	{
		if(tMessage.message == WM_QUIT)
		{
			this->m_iExitEngine = 1;
		}
		
		TranslateMessage(&tMessage);
		DispatchMessage(&tMessage);
	}
}

void CEngineOGL::SafeMemoryDeallocation(void *pMemoryPointer)
{
	if(pMemoryPointer)
	{
		free(pMemoryPointer);
	}
	pMemoryPointer = 0;
}

void CEngineOGL::Shutdown(void)
{
	ShowCursor(1);

	this->SafeMemoryDeallocation(m_poLight);
	this->SafeMemoryDeallocation(m_poCamera);

	this->ResetScreen();
}

////// SETTER
///////////////////////////////////////////////////////////////////////////////

////// GETTER
///////////////////////////////////////////////////////////////////////////////
