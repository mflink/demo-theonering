#pragma once

#include "..\..\shared\OpenGL.h"

#include "..\..\shared\LightOGL.h"		// Header for OpenGL-Lights
#include "..\..\shared\CameraOGL.h"		// Header for OpenGL-Cameras
#include "..\..\shared\FontOGL.h"		// Header for fonts in OpenGL
#include "..\..\shared\3dObjectOGL.h"	// Header for 3D-Objects in OpenGL

#define sFILENAME_THEONERING	"3dobjects/Ring,1.cob"

#define iMAXIMUM_LIGHTS			1
#define iMAXIMUM_CAMERAS		1

#define iCAMERA_DEFAULT			0

class CEngineOGL : public COpenGL
{
private:
	int				m_iExitEngine;
	CLightOGL		*m_poLight;
	CCameraOGL		*m_poCamera;
	CFontOGL		m_oFont;
	int				m_iFPS;
	C3DObjectOGL	m_oTheOneRing;
	VECTOR_F		m_tCameraPosition;

	void InitializeScene(void);
	void DrawScene(void);

	// Gibt von der Engine belegten Speicher frei.
	void Shutdown(void);
	// F�hrt die entsprechenden Tastatur-/Mausbefehle f�r die Engine aus.
	void ProcessInput(void);
	// F�hrt eine Initialisierung der Engine durch 
	// (Objekte laden, Positionen einstellen, ...)
	int InitializeEngine(void);

	// Spielt das Intro ab.
	void PlayIntro(HWND hWindow);

	// Pr�ft, ob eine Taste (Maus & Tastatur) gedr�ckt wurden.
	int KeyPressed(int iKeyNumber);

	// Pr�ft, ob eine Taste (Maus & Tastatur) gedr�ckt ist.
	int KeyDown(int iKeyNumber);
	// L��t das Betriebssystem seine Aufgaben durchf�hren.
	void DoOSEvents(void);

    // Versucht evtl. belegten Speicher sicher freizugeben
	void SafeMemoryDeallocation(void *pMemoryPointer);

public:
	CEngineOGL(void);
	~CEngineOGL(void);

	// F�hrt die Engine aus (beinhaltet die Hauptscheife).
	void Run(HWND hWindow);
};
